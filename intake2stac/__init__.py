from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Mostafa Hadizadeh"
__copyright__ = "Copyright (C) 2023 Karlsruher Institut für Technologie"
__credits__ = [
    "Mostafa Hadizadeh",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Karlsruher Institut für Technologie"
__email__ = "mostafa.hadizadeh@kit.edu"

__status__ = "Pre-Alpha"

__version__ = _version.get_versions()["version"]

__version__ = _version.get_versions()["version"]
